# Pisc Project

This project contains a Python script that reads a CSV file, performs basic data analysis, and outputs the results. The analysis results are displayed in the console, and two plots are generated and saved as PNG files.

## Requirements

- Python 3.x
- pandas
- matplotlib

## Installation
pip install pandas matplotlib


## Create CSV Files (salesDate.csv)

Date,Product,Sales,Region
2023-01-01,Product A,100,North
2023-01-02,Product B,150,South
2023-02-01,Product A,200,North
2023-02-03,Product C,300,West
2023-03-01,Product B,250,East
2023-03-05,Product C,350,West



## Run the script:
python data.py


import pandas as pd
import matplotlib.pyplot as plt

# Load the CSV file
df = pd.read_csv('salesData.csv')

print(df.head())

# Total Sales per Product
totalSalesPerProduct = df.groupby('Product')['Sales'].sum()
print("\nTotal Sales per Product:")
print(totalSalesPerProduct)

# Sales Trends Over Time
df['Date'] = pd.to_datetime(df['Date'])
salesTrendsOverTime = df.groupby('Date')['Sales'].sum()

# Plot total sales per product
plt.figure(figsize=(10, 6))
totalSalesPerProduct.plot(kind='bar', color='skyblue')
plt.title('Total Sales per Product')
plt.xlabel('Product')
plt.ylabel('Total Sales')
plt.savefig('totalSalesPerProduct.png')
plt.show()

# Plot sales trends over time
plt.figure(figsize=(10, 6))
salesTrendsOverTime.plot(kind='line', marker='o', color='blue')
plt.title('Sales Trends Over Time')
plt.xlabel('Date')
plt.ylabel('Sales')
plt.grid(True)
plt.savefig('salesTrendsOverTime.png')
plt.show()

